# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('forum', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProblemRevision',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('points', models.IntegerField(default=0)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('view_count', models.IntegerField(default=0)),
                ('title', models.CharField(max_length=254)),
                ('description', models.TextField()),
                ('source', models.TextField(blank=True)),
                ('curr_solution', models.TextField(blank=True)),
                ('lim_curr_solution', models.TextField(blank=True)),
                ('attempts', models.TextField(blank=True)),
                ('specific_goal', models.TextField(blank=True)),
                ('author', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='forum_created_problemrevisions')),
                ('problem', models.ForeignKey(to='forum.Problem', related_name='revisions')),
                ('voted_users', models.ManyToManyField(related_name='forum_liked_problemrevisions', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='SolutionRevision',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('points', models.IntegerField(default=0)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('view_count', models.IntegerField(default=0)),
                ('body', models.TextField()),
                ('author', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='forum_created_solutionrevisions')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AlterField(
            model_name='solution',
            name='problem',
            field=models.ForeignKey(to='forum.Problem', related_name='solutions'),
        ),
        migrations.AddField(
            model_name='solutionrevision',
            name='solution',
            field=models.ForeignKey(to='forum.Solution', related_name='revisions'),
        ),
        migrations.AddField(
            model_name='solutionrevision',
            name='voted_users',
            field=models.ManyToManyField(related_name='forum_liked_solutionrevisions', to=settings.AUTH_USER_MODEL),
        ),
    ]
