# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Area',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=254)),
                ('author', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='created_areas')),
            ],
        ),
        migrations.CreateModel(
            name='Goal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('points', models.IntegerField(default=0)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('view_count', models.IntegerField(default=0)),
                ('title', models.CharField(max_length=254)),
                ('description', models.TextField()),
                ('targets', models.TextField(blank=True)),
                ('achievements', models.TextField(blank=True)),
                ('author', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='forum_created_goals')),
                ('voted_users', models.ManyToManyField(related_name='forum_liked_goals', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='GoalRevision',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('points', models.IntegerField(default=0)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('view_count', models.IntegerField(default=0)),
                ('title', models.CharField(max_length=254)),
                ('description', models.TextField()),
                ('targets', models.TextField(blank=True)),
                ('achievements', models.TextField(blank=True)),
                ('author', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='forum_created_goalrevisions')),
                ('goal', models.ForeignKey(to='forum.Goal', related_name='revisions')),
                ('voted_users', models.ManyToManyField(related_name='forum_liked_goalrevisions', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Problem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('points', models.IntegerField(default=0)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('view_count', models.IntegerField(default=0)),
                ('title', models.CharField(max_length=254)),
                ('description', models.TextField()),
                ('source', models.TextField(blank=True)),
                ('curr_solution', models.TextField(blank=True)),
                ('lim_curr_solution', models.TextField(blank=True)),
                ('attempts', models.TextField(blank=True)),
                ('specific_goal', models.TextField(blank=True)),
                ('author', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='forum_created_problems')),
                ('goal', models.ForeignKey(blank=True, null=True, to='forum.Goal', related_name='related_problems')),
                ('voted_users', models.ManyToManyField(related_name='forum_liked_problems', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Solution',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('points', models.IntegerField(default=0)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('view_count', models.IntegerField(default=0)),
                ('body', models.TextField()),
                ('author', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='forum_created_solutions')),
                ('problem', models.ForeignKey(to='forum.Problem', related_name='related_solutions')),
                ('voted_users', models.ManyToManyField(related_name='forum_liked_solutions', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='area',
            name='goal',
            field=models.ManyToManyField(related_name='related_areas', to='forum.Goal'),
        ),
        migrations.AddField(
            model_name='area',
            name='problem',
            field=models.ManyToManyField(related_name='related_areas', to='forum.Problem'),
        ),
    ]
