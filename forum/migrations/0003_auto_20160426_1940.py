# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('forum', '0002_auto_20160328_1110'),
    ]

    operations = [
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('points', models.IntegerField(default=0)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('view_count', models.IntegerField(default=0)),
                ('body', models.TextField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AlterField(
            model_name='area',
            name='goal',
            field=models.ManyToManyField(related_name='areas', to='forum.Goal'),
        ),
        migrations.AlterField(
            model_name='area',
            name='problem',
            field=models.ManyToManyField(related_name='areas', to='forum.Problem'),
        ),
        migrations.AlterField(
            model_name='problem',
            name='goal',
            field=models.ForeignKey(blank=True, related_name='problems', to='forum.Goal', null=True),
        ),
        migrations.CreateModel(
            name='GoalQuestion',
            fields=[
                ('question_ptr', models.OneToOneField(auto_created=True, parent_link=True, serialize=False, to='forum.Question', primary_key=True)),
                ('post', models.ForeignKey(related_name='questions', to='forum.Goal')),
            ],
            options={
                'abstract': False,
            },
            bases=('forum.question',),
        ),
        migrations.CreateModel(
            name='ProblemQuestion',
            fields=[
                ('question_ptr', models.OneToOneField(auto_created=True, parent_link=True, serialize=False, to='forum.Question', primary_key=True)),
                ('post', models.ForeignKey(related_name='questions', to='forum.Problem')),
            ],
            options={
                'abstract': False,
            },
            bases=('forum.question',),
        ),
        migrations.CreateModel(
            name='SolutionQuestion',
            fields=[
                ('question_ptr', models.OneToOneField(auto_created=True, parent_link=True, serialize=False, to='forum.Question', primary_key=True)),
                ('post', models.ForeignKey(related_name='questions', to='forum.Solution')),
            ],
            options={
                'abstract': False,
            },
            bases=('forum.question',),
        ),
        migrations.AddField(
            model_name='question',
            name='author',
            field=models.ForeignKey(related_name='forum_created_questions', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='question',
            name='voted_users',
            field=models.ManyToManyField(related_name='forum_liked_questions', to=settings.AUTH_USER_MODEL),
        ),
    ]
