from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^problems/(?P<problem_id>[0-9]*)$', views.thread, name='thread'),
    url(r'^solutions/(?P<solution_id>[0-9]+)/edit/(?P<rev_val>[0-9]*)$', views.edit_solution, name='edit_solution'),
    url(r'^problems/post$', views.post_problem, name='post_problem'),
    url(r'^problems/(?P<problem_id>[0-9]+)/edit/(?P<rev_val>[0-9]*)$', views.edit_problem, name='edit_problem'),
    url(r'^search-goal/$', views.search_goal, name='search_goal'),
    url(r'^goals/post$', views.post_goal, name='post_goal'),
    url(r'^goals/(?P<goal_id>[0-9]+)/edit/(?P<rev_val>[0-9]*)$', views.edit_goal, name='edit_goal'),
    url(r'^goals/(?P<goal_id>[0-9]+)$', views.goal, name='goal'),
    url(r'^accounts/profile/$', views.profile, name='profile'),
    url(r'^qs/(?P<post_id>[0-9]+)/(?P<post_type>[0-9]+)$', views.question, name='question'),
]