from django.forms import ModelForm, Textarea
from forum.models import ProblemQuestion, SolutionQuestion, GoalQuestion

class ProblemQuestionForm(ModelForm):

    class Meta:
        model = ProblemQuestion
        fields = ['body']
        widgets = {
            'body': Textarea(attrs={'cols': 80, 'rows': 2}),
        }

class SolutionQuestionForm(ModelForm):

    class Meta:
        model = SolutionQuestion
        fields = ['body']
        widgets = {
            'body': Textarea(attrs={'cols': 80, 'rows': 2}),
        }

class GoalQuestionForm(ModelForm):

    class Meta:
        model = GoalQuestion
        fields = ['body']
        widgets = {
            'body': Textarea(attrs={'cols': 80, 'rows': 2}),
        }