from django.forms import ModelForm, ModelChoiceField
from forum.models import Problem

class ProblemForm(ModelForm):

    class Meta:
        model = Problem
        fields = ['goal', 'title', 'description', 'source', 'curr_solution', 'lim_curr_solution', 'attempts', 'specific_goal']

class EditProblemForm(ProblemForm):

    revisions = ModelChoiceField(queryset=None, empty_label=None)

    def __init__(self, *args, **kwargs):
        self.problem = kwargs.pop('problem')
        super(ProblemForm, self).__init__(*args, **kwargs)
        self.fields['revisions'].queryset = self.problem.revisions.all()