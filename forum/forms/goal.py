from django.forms import ModelForm, ModelChoiceField
from forum.models import Goal

class GoalForm(ModelForm):

    class Meta:
        model = Goal
        fields = ['title', 'description', 'targets', 'achievements']


class EditGoalForm(GoalForm):

    revisions = ModelChoiceField(queryset=None, empty_label=None)

    def __init__(self, *args, **kwargs):
        self.goal = kwargs.pop('goal')
        super(GoalForm, self).__init__(*args, **kwargs)
        self.fields['revisions'].queryset = self.goal.revisions.all()
