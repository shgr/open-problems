from forum.forms.goal import GoalForm, EditGoalForm
from forum.forms.problem import ProblemForm, EditProblemForm
from forum.forms.solution import SolutionForm, EditSolutionForm
from forum.forms.question import ProblemQuestionForm, SolutionQuestionForm, GoalQuestionForm