from django.forms import ModelForm, ModelChoiceField
from forum.models import Solution

class SolutionForm(ModelForm):

    class Meta:
        model = Solution
        fields = ['body']

class EditSolutionForm(SolutionForm):

    revisions = ModelChoiceField(queryset=None, empty_label=None)

    def __init__(self, *args, **kwargs):
        self.solution = kwargs.pop('solution')
        super(SolutionForm, self).__init__(*args, **kwargs)
        self.fields['revisions'].queryset = self.solution.revisions.all()