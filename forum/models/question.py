from django.db import models
from forum.models.base import Post
from forum.models import Problem, Solution, Goal

class Question(Post):
    body = models.TextField()

    def __str__(self):
        return self.body

class ProblemQuestion(Question):
    post = models.ForeignKey(Problem, related_name='questions')

class SolutionQuestion(Question):
    post = models.ForeignKey(Solution, related_name='questions')

class GoalQuestion(Question):
    post = models.ForeignKey(Goal, related_name='questions')