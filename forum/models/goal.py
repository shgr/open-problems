from django.db import models
from forum.models.base import Post

class GoalGeneric(models.Model):
    title = models.CharField(max_length=254)
    description = models.TextField()

    targets = models.TextField(blank=True)
    achievements = models.TextField(blank=True)

    def __str__(self):
        return self.title

    class Meta:
        abstract = True

class Goal(Post, GoalGeneric):
    pass

class GoalRevision(Post, GoalGeneric):
    goal = models.ForeignKey(Goal, related_name="revisions")