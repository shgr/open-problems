from django.db import models
from forum.models.base import Post
from forum.models import Problem


class SolutionGeneric(models.Model):

    body = models.TextField()

    class Meta:
        abstract = True

    def __str__(self):
        return self.body

class Solution(Post, SolutionGeneric):
    problem = models.ForeignKey(Problem, related_name='solutions')

class SolutionRevision(Post, SolutionGeneric):
    solution = models.ForeignKey(Solution, related_name="revisions")