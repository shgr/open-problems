from django.db import models
from forum.models.base import Post
from forum.models import Goal

class ProblemGeneric(models.Model):
    title = models.CharField(max_length=254)
    description = models.TextField()
    source = models.TextField(blank=True)
    curr_solution = models.TextField(blank=True)
    lim_curr_solution = models.TextField(blank=True)
    attempts = models.TextField(blank=True)
    specific_goal = models.TextField(blank=True)

    def __str__(self):
        return self.title

    class Meta:
        abstract = True

class Problem(Post, ProblemGeneric):
    goal = models.ForeignKey(Goal, null=True, blank=True, related_name='problems')

class ProblemRevision(Post, ProblemGeneric):
    problem = models.ForeignKey(Problem, related_name="revisions")


