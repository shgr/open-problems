from django.db import models
from django.contrib.auth.models import User
from forum.models import Goal, Problem

class Area(models.Model):
    """Popularly known as tags"""

    name = models.CharField(max_length=254)
    author = models.ForeignKey(User, related_name='created_areas')

    problem = models.ManyToManyField(Problem, related_name='areas')
    goal = models.ManyToManyField(Goal, related_name='areas')

    def __str__(self):
        return self.name

    class Meta:
        app_label = 'forum'