from django.db import models
from django.contrib.auth.models import User

class Post(models.Model):

    author = models.ForeignKey(User, related_name="%(app_label)s_created_%(class)ss")
    voted_users = models.ManyToManyField(User, related_name="%(app_label)s_liked_%(class)ss")
    points = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    view_count = models.IntegerField(default=0)

    class Meta:
        abstract = True
        app_label = 'forum'