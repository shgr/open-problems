from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse

from forum.models import Problem, ProblemRevision, Goal, GoalRevision, Solution,\
                        SolutionRevision
from forum.forms import ProblemForm, EditProblemForm, GoalForm, EditGoalForm,\
                        SolutionForm, EditSolutionForm, ProblemQuestionForm,\
                        SolutionQuestionForm, GoalQuestionForm


def index(request):

    problems = Problem.objects.all()

    return render(request, 'index.html', {'problems': problems
                                        })



def populate_goal(revision, goal):
    data = {'title': revision.title, 'description': revision.description, 'targets': revision.targets,
                'achievements': revision.achievements}
    form = EditGoalForm(goal=goal, initial=data)
    return form

def populate_problem(revision, problem):
    data = {'title': revision.title, 'description': revision.description,
                            'source': revision.source, 'curr_solution': revision.curr_solution,
                            'lim_curr_solution': revision.lim_curr_solution, 'attempts': revision.attempts,
                            'specific_goal': revision.specific_goal}
    form = EditProblemForm(problem=problem, initial=data)
    return form

def populate_solution(revision, solution):
    data = {'body': revision.body}
    form = EditSolutionForm(solution=solution, initial=data)
    return form

def populate_form(rev_val, post):
    if rev_val:
        if post.__class__ == Solution:
            revision = get_object_or_404(SolutionRevision, pk=rev_val)
            form = populate_solution(revision, post)
            return form
        elif post.__class__ == Problem:
            revision = get_object_or_404(ProblemRevision, pk=rev_val)
            form = populate_problem(revision, post)
            return form
        else:
            revision = get_object_or_404(GoalRevision, pk=rev_val)
            form = populate_goal(revision, post)
            return form
    else:
        #if no rev_val present in GET request, pick the latest revision as default
        revision = post.revisions.all()[0]
        if post.__class__ == Solution:
            form = populate_solution(revision, post)
            return form
        elif post.__class__ == Problem:
            form = populate_problem(revision, post)
            return form
        else:
            form = populate_goal(revision, post)
            return form


def thread(request, problem_id):

    problem = get_object_or_404(Problem, pk=problem_id)

    if request.method == 'POST':
        form = SolutionForm(request.POST)
        if form.is_valid():
            solution = form.save(commit=False)
            init_rev = SolutionRevision(body=solution.body, author=request.user)
            return handle_form(solution, init_rev, request, problem)[0]
    else:
        form = SolutionForm()



    solutions = problem.solutions.order_by('points')

    related_problems = problem.goal.problems.exclude(pk=problem_id)

    problem_questions = problem.questions.all()

    pq_form = ProblemQuestionForm()

    sq_form = SolutionQuestionForm()

    return render(request, 'problems/thread.html', {'solutions': solutions,
                                                    'problem': problem,
                                                    'related_problems': related_problems,
                                                    'body': form['body'],
                                                    'problem_questions': problem_questions,
                                                    'pq_body': pq_form['body'],
                                                    'sq_body': sq_form['body'],
                                                    })



@login_required
def edit_solution(request, solution_id, rev_val):

    solution = get_object_or_404(Solution, pk=solution_id)

    if request.method == 'POST':
        form = SolutionForm(request.POST)
        if form.is_valid():
            rev = form.save(commit=False)
            rev = SolutionRevision(body=rev.body, author=request.user)
            return handle_form(solution, rev)[0]
    else:
        form = populate_form(rev_val, solution)

    return render(request, 'solutions/edit.html',{'revisions': form['revisions'],
                                                  'body': form['body'],
                                                  'solution_id': solution_id,
                                            })

def handle_form(post, rev, request=None, problem=None):

    name = post.__class__.__name__.lower()

    if request:
        post.author = request.user

    if problem:
        problem.solutions.add(post)

    post.save()
    post.revisions.add(rev)
    if name == 'solution':
        post.body = rev.body
    elif name == 'problem':
        post.title, post.description, post.source, post.curr_solution, post.lim_curr_solution,\
        post.attempts, post.specific_goal = rev.title, rev.description, rev.source, \
        rev.curr_solution, rev.lim_curr_solution, rev.attempts, rev.specific_goal
    else:
        post.title, post.description, post.targets, post.achievements = rev.title, \
        rev.description, rev.targets, rev.achievements

    post.save()


    return (HttpResponseRedirect(reverse('forum:{0}'.format(name if name == 'goal' else 'thread'),
                                 args=(post.id if name != 'solution' else post.problem.id,))),)

@login_required
def post_problem(request):

    if request.method == 'POST':
        form = ProblemForm(request.POST)
        if form.is_valid():
            problem = form.save(commit=False)
            init_rev = ProblemRevision(title=problem.title, description=problem.description,
                            source=problem.source, curr_solution=problem.curr_solution,
                            lim_curr_solution=problem.lim_curr_solution, attempts=problem.attempts,
                            specific_goal=problem.specific_goal, author=request.user)
            return handle_form(problem, init_rev, request)[0]
    else:
        form = ProblemForm()

    return render(request, 'problems/post.html',{'goal': form['goal'],
                                                'title': form['title'],
                                                'description': form['description'],
                                                'source': form['source'],
                                                'curr_solution': form['curr_solution'],
                                                'lim_curr_solution': form['lim_curr_solution'],
                                                'attempts': form['attempts'],
                                                'specific_goal': form['specific_goal'],
                                                })


@login_required
def edit_problem(request, problem_id, rev_val):

    problem = get_object_or_404(Problem, pk=problem_id)

    if request.method == 'POST':
        form = ProblemForm(request.POST)
        if form.is_valid():
            rev = form.save(commit=False)
            rev = ProblemRevision(title=rev.title, description=rev.description,
                            source=rev.source, curr_solution=rev.curr_solution,
                            lim_curr_solution=rev.lim_curr_solution, attempts=rev.attempts,
                            specific_goal=rev.specific_goal, author=request.user)
            return handle_form(problem, rev)[0]
    else:
        form = populate_form(rev_val, problem)

    return render(request, 'problems/edit.html',{'revisions': form['revisions'],
                                                'goal': form['goal'],
                                                'title': form['title'],
                                                'description': form['description'],
                                                'source': form['source'],
                                                'curr_solution': form['curr_solution'],
                                                'lim_curr_solution': form['lim_curr_solution'],
                                                'attempts': form['attempts'],
                                                'specific_goal': form['specific_goal'],
                                                'problem_id': problem_id,
                                                })


@login_required
def post_goal(request):
    if request.method == 'POST':
        form = GoalForm(request.POST)
        if form.is_valid():
            goal = form.save(commit=False)
            init_rev = GoalRevision(title=goal.title, description=goal.description,
                                    targets=goal.targets, achievements=goal.achievements,
                                    author=request.user)
            return handle_form(goal, init_rev, request)[0]
    else:
        form = GoalForm()

    return render(request, 'goals/post.html', {'title': form['title'],
                                              'description': form['description'],
                                              'targets': form['targets'],
                                              'achievements': form['achievements'],
                                            })


@login_required
def edit_goal(request, goal_id, rev_val):

    goal = get_object_or_404(Goal, pk=goal_id)

    if request.method == 'POST':
        form = GoalForm(request.POST)
        if form.is_valid():
            rev = form.save(commit=False)
            #right now rev is an instance of Goal class, but this line overrides its class
            rev = GoalRevision(title=rev.title, description=rev.description, targets=rev.targets,
                                    achievements=rev.achievements, author=request.user)
            return handle_form(goal, rev)[0]
    else:
        form = populate_form(rev_val, goal)

    return render(request, 'goals/edit.html', {'revisions': form['revisions'],
                                              'title': form['title'],
                                              'description': form['description'],
                                              'targets': form['targets'],
                                              'achievements': form['achievements'],
                                              'goal_id': goal_id,
                                              })


def search_goal(request):
    if request.method == 'POST':
        search_text = request.POST['search_text']
    else:
        search_text = ''
    goals = Goal.objects.filter(title__contains=search_text)

    return render(request, 'goals/found-goals.html', {'goals': goals})


def goal(request, goal_id):

    goal = get_object_or_404(Goal, pk=goal_id)
    return render(request, 'goals/goal.html', {'goal': goal})


def profile(request):

    return render(request, 'profile.html', {})

def question_add(form, request, post):
    question = form.save(commit=False)
    question.author = request.user
    post.questions.add(question)
    question.save()

def question(request, post_id, post_type):

    if post_type == '1':
        form = ProblemQuestionForm(request.POST)
        if form.is_valid():
            problem = get_object_or_404(Problem, pk=post_id)
            question_add(form, request, problem)
    elif post_type == '2':
        form = SolutionQuestionForm(request.POST)
        if form.is_valid():
            solution = get_object_or_404(Solution, pk=post_id)
            question_add(form, request, solution)
    else:
        form = GoalQuestionForm(request.POST)
        if form.is_valid():
            goal = get_object_or_404(Goal, pk=post_id)
            question_add(form, request, goal)
    return HttpResponse()