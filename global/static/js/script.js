$(function () {

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');


$('#id_goal').keyup(function() {

    $.ajax({
        type: "POST",
        url: "/search-goal/",
        data: {
            'search_text': $('#id_goal').val()
        },
        headers: {
            'X-CSRFToken': csrftoken
        },
        dataType: 'html',
    })
    .done(function(data, textStatus, jqXHR) {
        $('#found-goals').html(data);
    });
});


$( "#id_revisions" ).change(function() {

    $.ajax({
        data: {
            'rev_val': $("select option:selected").val()
        },
        headers: {
            'X-CSRFToken': csrftoken
        },
        dataType: 'html',
    })/*
    .done(function(data, textStatus, jqXHR) {

    });*/

});

$('.addComment').click(function(){


  var element = $(this);
  var id = element.attr("id");

  $('#commentBox'+id).slideToggle(200);
  $('#comment'+id).focus();
  $('#comment'+id).val('');



 });

 $('.comBtn').click(function(){

  var element = $(this);
  var cid = element.attr("id");

  var comment=$('#comment'+cid).val();
  var datasend = 'com='+ comment + '&pid=' + cid;

  if(comment==""){

   alert("Please enter the comment");
   $('#comment'+cid).focus();

  }else{

  $.ajax({

   type:"POST",
   url:"comment.php",
   data:datasend,
   cache:false,
   success:function(){

     $('#comment'+cid).val('');

   }


  });

  }
  return false;

  });

});
