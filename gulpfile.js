var gulp = require('gulp'),
    less = require('gulp-less'),
    LessPluginCleanCSS = require('less-plugin-clean-css'),
    LessPluginAutoPrefix = require('less-plugin-autoprefix'),
    LessPluginBootstrap = require('less-plugin-bootstrap'),
    browserSync = require('browser-sync').create();


cleancss = new LessPluginCleanCSS({ advanced: true });
autoprefix = new LessPluginAutoPrefix({browsers: ["last 2 versions"]});
bootstrapPlugin = new LessPluginBootstrap();

gulp.task('default', ['serve']);

gulp.task('serve', ['styles'], function() {

    browserSync.init({
        proxy: "localhost:8000"
    });

    gulp.watch('global/static/src/less/*.less', ['styles']);
    gulp.watch(["global/**/*.html", "forum/**/*.html"]).on('change', browserSync.reload);
});

gulp.task('styles', function () {
    gulp.src('global/static/src/less/styles.less')
    .pipe(less({
        plugins:[autoprefix, cleancss, bootstrapPlugin]
    }))
    // .pipe(concat('common.css'))
    // .pipe(rename({suffix: ".min"}))
    .pipe(gulp.dest('global/static/css'))
    .pipe(browserSync.stream());
});